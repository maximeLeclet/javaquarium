package fr.univlittoral.javaquarium.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univlittoral.javaquarium.bo.PoissonBO;
import fr.univlittoral.javaquarium.dos.PoissonDO;
import fr.univlittoral.javaquarium.dto.ListePoissonsDTO;
import fr.univlittoral.javaquarium.dto.PoissonDTO;

@RestController
@RequestMapping(value = "/poissons")
public class ListePoissonsController {
	
	@Autowired
	private PoissonBO poissonBO;
	
	@RequestMapping(method = RequestMethod.GET)
	public ListePoissonsDTO getAll() {
		
		final ListePoissonsDTO listePoissonsDTO = new ListePoissonsDTO();
		
		final List<PoissonDO> poissonsDO = poissonBO.findAll();
		
		for(final PoissonDO poissonDO : poissonsDO) {
			
			PoissonDTO poissonDTO = new PoissonDTO();
			
			poissonDTO.setNom(poissonDO.getEspece());
			poissonDTO.setDescription(poissonDO.getDesc1());
			poissonDTO.setDimension(poissonDO.getLargeur() + "x" + poissonDO.getLongueur());
			poissonDTO.setPrix(poissonDO.getPrix());
			poissonDTO.setCouleur(poissonDO.getCouleur());
		
			listePoissonsDTO.getPoissons().add(poissonDTO);
			
		}
		
		return listePoissonsDTO;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/new")
	public void newPoisson(final PoissonDTO poissonDTO) {
		
		final PoissonDO poissonDO = new PoissonDO();
		
		poissonDO.setEspece(poissonDTO.getNom());
		poissonDO.setDesc1(poissonDTO.getDescription());
		
		final String[] dimensions = poissonDTO.getDimension().split("x");
		poissonDO.setLongueur(Double.valueOf(dimensions[0]));
		poissonDO.setLargeur(Double.valueOf(dimensions[1]));
		
		poissonDO.setPrix(poissonDTO.getPrix());
		poissonDO.setCouleur(poissonDTO.getCouleur());
		
		poissonBO.addPoisson(poissonDO);
		
	}

}
