package fr.univlittoral.javaquarium.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.provider.HibernateUtils;
import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.dos.PoissonDO;

@Component
public class PoissonDAO {
	
	@Autowired
	private EntityManager em;

	/**
	 * Retourne la liste de tous les poissons
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PoissonDO> findAll() {
		final Query q = em.createQuery("from PoissonDO");
		return q.getResultList();
	}
	
	/**
	 * Enregistre un poisson en base 
	 * 
	 * @param poissonDO
	 */
	@Transactional
	public void addPoisson(final PoissonDO poissonDO) {
		em.persist(poissonDO);
		em.flush();
	}
	
}
