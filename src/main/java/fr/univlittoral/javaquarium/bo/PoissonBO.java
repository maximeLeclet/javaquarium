package fr.univlittoral.javaquarium.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univlittoral.javaquarium.dao.PoissonDAO;
import fr.univlittoral.javaquarium.dos.PoissonDO;

@Service
public class PoissonBO {

	@Autowired
	private PoissonDAO poissonDAO;
	
	public PoissonBO() {
		poissonDAO = new PoissonDAO();
	}
	
	public List<PoissonDO> findAll() {
		return poissonDAO.findAll();
	}
	
	public void addPoisson(final PoissonDO poissonDO) {
		poissonDAO.addPoisson(poissonDO);
	}
	
}
