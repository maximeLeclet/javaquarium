package fr.univlittoral.javaquarium.dos;

import javax.persistence.*;

@Entity
@Table(name="P_POISSONS")
public class PoissonDO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="P_ID")
	private Integer id;
	
	@Column(name="P_ESPECE", nullable=false)
	private String espece;
	
	@Column(name="P_DESC1")
	private String desc1;
	
	@Column(name="P_DESC2")
	private String desc2;
	
	@Column(name="P_DESC3")
	private String desc3;
	
	@Column(name="P_COULEUR")
	private String couleur;
	
	@Column(name="P_LARGEUR")
	private double largeur;
	
	@Column(name="P_LONGUEUR")
	private double longueur;
	
	@Column(name="P_PRIX")
	private double prix;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getEspece() {
		return espece;
	}

	public void setEspece(final String espece) {
		this.espece = espece;
	}

	public String getDesc1() {
		return desc1;
	}

	public void setDesc1(final String desc1) {
		this.desc1 = desc1;
	}

	public String getDesc2() {
		return desc2;
	}

	public void setDesc2(final String desc2) {
		this.desc2 = desc2;
	}

	public String getDesc3() {
		return desc3;
	}

	public void setDesc3(final String desc3) {
		this.desc3 = desc3;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(final String couleur) {
		this.couleur = couleur;
	}

	public double getLargeur() {
		return largeur;
	}

	public void setLargeur(final double largeur) {
		this.largeur = largeur;
	}

	public double getLongueur() {
		return longueur;
	}

	public void setLongueur(final double longueur) {
		this.longueur = longueur;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(final double prix) {
		this.prix = prix;
	}
	
}
