package fr.univlittoral.javaquarium.dto;

import java.util.List;
import java.util.ArrayList;

public class ListePoissonsDTO {

	private List<PoissonDTO> poissonsDTO; 
	
	public ListePoissonsDTO() {
		poissonsDTO = new ArrayList<PoissonDTO>();
	}
	
	public List<PoissonDTO> getPoissons() {
		return poissonsDTO;
	}
	
}
