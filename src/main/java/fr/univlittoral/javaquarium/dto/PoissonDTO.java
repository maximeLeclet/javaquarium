package fr.univlittoral.javaquarium.dto;

public class PoissonDTO {
	
	private String nom;
	
	private String description;
	
	private String couleur;
	
	private String dimension;
	
	private double prix;

	public String getNom() {
		return nom;
	}

	public void setNom(final String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(final String couleur) {
		this.couleur = couleur;
	}

	public String getDimension() {
		return dimension;
	}

	public void setDimension(final String dimension) {
		this.dimension = dimension;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(final double prix) {
		this.prix = prix;
	}

}
